# Update the rust toolchain to the latest version
rustup update

# Compile the project
cargo build --release

